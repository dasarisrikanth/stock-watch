import {
  LOAD_PORTFOLIO_INPROGRESS,
  LOAD_PORTFOLIO_ERROR,
  LOAD_PORTFOLIO_SUCCESS,
} from './../constants/ActionTypes';
import axios from 'axios';
import {getData} from '../utils/AsyncStorageWrapper';
import {PORTFOLIO} from './../constants/storage-keys';
import _ from 'lodash';
import {UrlBuilder} from '../utils/UrlBuilder';
import {ENDPOINT} from './../constants/Endpoint';

export const loadPortfolioInProgressAction = () => {
  return {
    type: LOAD_PORTFOLIO_INPROGRESS 
  };
};

export const loadPortfolioErrorAction = (error) => {
  return {
    type: LOAD_PORTFOLIO_ERROR,
    error
  };
};

export const loadPortfolioSuccessAction = (data) => {
  return {
    type: LOAD_PORTFOLIO_SUCCESS,
    data
  };
};


export const getPortfolio = async (dispatch) => {
  // console.log("getPortfolio Action");
  dispatch(loadPortfolioInProgressAction());
  try {
    const existingData = await getData(PORTFOLIO);
    console.log("portfolio items from local storage",existingData);
    if(_.isArray(existingData)) {
      const urlObj = new UrlBuilder()
        .setBaseUrl()
        .setEndpoint(ENDPOINT.WATCHLIST_DETAILS)
        .setCustomQuery('symbols', existingData.join(','))
        .setCustomQuery('types', 'quote,news,chart')
        .setCustomQuery('range', '1m')
        .setCustomQuery('last', 5)
        .setPublicToken()
        .build();
      console.log("url call",urlObj.getUrl());
      await axios.get(urlObj.getUrl()).then((resp)=>{
        console.log(resp);
        dispatch(loadPortfolioSuccessAction(resp.data));
      });
    }
  } catch (e) {
    console.error(e);
    dispatch(loadPortfolioErrorAction(e));
  }
};
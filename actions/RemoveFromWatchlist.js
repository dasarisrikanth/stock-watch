import React from 'react';
import _ from 'lodash';
import {
  REMOVE_FROM_WATCHLIST_INPROGRESS,
  REMOVE_FROM_WATCHLIST_ERROR,
  REMOVE_FROM_WATCHLIST_SUCCESS
} from './../constants/ActionTypes';
import {
  storeData,
  getData,
  removeItemValue
} from '../utils/AsyncStorageWrapper';

import { WATCHLIST } from '../constants/storage-keys';

export const removeFromWatchlistInProgressAction = () => {
  return {
    type: REMOVE_FROM_WATCHLIST_INPROGRESS
  }
}

export const removeFromWatchlistErrorAction = (error) => {
  return {
    type: REMOVE_FROM_WATCHLIST_ERROR,
    error
  }
}

export const removeFromWatchlistSuccessAction = (data) => {
  return {
    type: REMOVE_FROM_WATCHLIST_SUCCESS,
    data
  }
}

export const removeFromWatchlist = async (ticker, dispatch) => {
  console.log("removeFromWatchlist -> ticker ",ticker);
  dispatch(removeFromWatchlistInProgressAction());
  if(ticker) {
    try{
      const existingData = await getData(WATCHLIST);
      let data = [];
      if (existingData) {
        data = [...existingData, ticker];
      } else {
        data = [ticker];
      }
      console.log("Before Deleting ->",data);
      _.remove(data,(x)=>{ return x.toLowerCase()===ticker.toLowerCase()});
      console.log("After Deleting ->",data);
      await storeData(WATCHLIST, data);
      dispatch(removeFromWatchlistSuccessAction(data));
    } catch(err) {
      dispatch(removeFromWatchlistErrorAction(err));
    }
  }
}
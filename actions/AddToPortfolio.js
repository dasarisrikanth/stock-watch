import React from 'react';
import axios from 'axios';
import _ from 'lodash';
import {
  ADD_TO_PORTFOLIO_INPROGRESS,
  ADD_TO_PORTFOLIO_ERROR,
  ADD_TO_PORTFOLIO_SUCCESS,
} from './../constants/ActionTypes';
import {
  storeData,
  getData,
  removeItemValue,
} from '../utils/AsyncStorageWrapper';
import { PORTFOLIO } from '../constants/storage-keys';

export const addToPortfolioInProgressAction = () => {
  return {
    type: ADD_TO_PORTFOLIO_INPROGRESS,
  };
};

export const addToPortfolioErrorAction = (error) => {
  return {
    type: ADD_TO_PORTFOLIO_ERROR,
    error,
  };
};

export const addToPortfolioSuccessAction = (data) => {
  return {
    type: ADD_TO_PORTFOLIO_SUCCESS,
    data
  };
};

export const addToPortfolio = async (ticker, dispatch) => {
  // console.log("AddToPortfolio Action",ticker);
  dispatch(addToPortfolioInProgressAction());
  if(ticker) {
    try {
      const existingData = await getData(PORTFOLIO);
      let data = [];
      if(existingData) {
        data = [...existingData, ticker]
      } else {
        data = [ticker]
      }
      // console.log(data);
      await storeData(PORTFOLIO, _.uniq(data));
      dispatch(addToPortfolioSuccessAction(data));
    } catch (err) {
      // console.log(err);
      dispatch(addToPortfolioErrorAction(err));
    }
  }  
};

import React from 'react';
import _ from 'loadsh';
import {
  REMOVE_FROM_PORTFOLIO_ERROR,
  REMOVE_FROM_PORTFOLIO_SUCCESS,
  REMOVE_FROM_PORTFOLIO_INPROGRESS
} from './../constants/ActionTypes';
import {
  removeItemValue
} from '../utils/AsyncStorageWrapper';

import { PORTFOLIO } from '../constants/storage-keys';

export const removeFromPortfolioInProgressAction = () => {
  return {
    type: REMOVE_FROM_PORTFOLIO_INPROGRESS
  }
}

export const removeFromPortfolioErrorAction = (error) => {
  return {
    type: REMOVE_FROM_PORTFOLIO_ERROR,
    error
  }
}

export const removeFromPortfolioSuccessAction = (data) => {
  return {
    type: REMOVE_FROM_PORTFOLIO_SUCCESS,
    data
  }
}

export const removeFromPortfolio = async (ticker, dispatch) => {
  dispatch(removeFromPortfolioInProgressAction());
  if(ticker) {
    try{

    } catch(err) {
      
    }
  }
}
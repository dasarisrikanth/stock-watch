import React, {useState} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  FlatList,
  TextInput,
  TouchableOpacity,
  Animated
} from 'react-native';
import FeatherIcon from 'react-native-vector-icons/Feather';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import {withTheme, Chip} from 'react-native-paper';
 
import Swipeable from 'react-native-gesture-handler/Swipeable';
import Analysis from './Analysis';

// https://www.reactnativeschool.com/swipe-gestures-with-react-native-gesture-handler
// import {GestureHandler} from 'expo';
// const {Swipeable} = GestureHandler;

const SwipableListItem = ({item, theme, OnSelect, onRightPress }) => {
  const [selectedItem, setSelectedItem] = useState();
  // const onPress = (item) => {
  //   console.log(item);
  //   setSelectedItem(item);
  // };

  // const LeftActions = (progress, dragX) => {
  //   const scale = dragX.interpolate({
  //     inputRange: [0, 100],
  //     outputRange: [0, 1],
  //     extrapolate: 'clamp',
  //   });
  //   return (
  //     <View style={styles.leftAction}>
  //       <Animated.Text style={[styles.actionText, { transform: [{ scale }] }]}>
  //         Add to Cart
  //       </Animated.Text>
  //     </View>
  //   );
  // };

  const RightActions = ({ progress, dragX, onPress }) => {
    const scale = dragX.interpolate({
      inputRange: [-100, 0],
      outputRange: [1, 0],
      extrapolate: 'clamp',
    });
    return (
      <TouchableOpacity onPress={onPress}>
        <View style={styles.rightAction}>
          <Animated.Text style={[styles.actionText, { transform: [{ scale }] }]}>
            Delete
          </Animated.Text>
        </View>
      </TouchableOpacity>
    );
  };

  const styles = StyleSheet.create({
    // conatiner: {
    //   padding: 15,
    //   backgroundColor: theme.colors.surface,
    //   borderBottomWidth: 1,
    //   borderBottomColor: '#333',
    // },
    itemView: {
      flexDirection: 'row',
      padding: 10,
      width: '100%',
      // justifyContent: 'space-between',
      alignItems: 'center',
      borderBottomWidth: 1,
      borderBottomColor: '#ddd',
    },
    itemViewStats: {
      flexDirection: 'row',
      // justifyContent: 'space-around',
      padding: 3,
      margin: 3,
      fontSize: 10,
      color: theme.colors.primary,
    },
    ticker: {
      color: theme.colors.accent,
      backgroundColor: theme.colors.primary,
      // fontSize: 14,
      margin: 1,
    },
    tickerName: {
      // color: '#ffffff',
      color: theme.colors.accent,
      fontSize: 14,
      // textDecorationStyle: 'dotted',
      // flexWrap: 'wrap',
      marginLeft: 2,
      marginRight: 1,
    },
    textDetails: {
      fontSize: 10,
      color: theme.colors.primary,
      margin: 1,
    },
    textNumbers: {
      fontSize: 10,
      color: theme.colors.accent,
      margin: 1,
    },
    container: {
      backgroundColor: '#fff',
      paddingHorizontal: 10,
      paddingVertical: 20,
    },
    text: {
      color: '#4a4a4a',
      fontSize: 15,
    },
    separator: {
      flex: 1,
      height: 1,
      backgroundColor: '#e4e4e4',
      marginLeft: 10,
    },
    leftAction: {
      backgroundColor: '#388e3c',
      justifyContent: 'center',
      flex: 1,
    },
    rightAction: {
      backgroundColor: '#dd2c00',
      justifyContent: 'center',
      // flex: 1,
      alignItems: 'flex-end',
    },
    actionText: {
      color: '#fff',
      fontWeight: '600',
      padding: 20,
    },
  });

  return (
    <Swipeable 
      style={styles.conatiner}
      // renderLeftActions={LeftActions}
      // onSwipeableLeftOpen={onSwipeFromLeft}
      renderRightActions={(progress, dragX) => {
        // console.log("progress",progress);
        // console.log("dragX",dragX);
        return (
        <RightActions progress={progress} dragX={dragX} onPress={ ()=>onRightPress(item)} />
        )
      }}
    >
       <TouchableOpacity
        keyExtract={item.ticker}
        style={styles.conatiner}
        onPress={() => OnSelect(item)}
        >
        <View key={item.ticker} style={styles.itemView}>
          <Chip style={styles.ticker}>{item.ticker}</Chip>
          {/* <Text style={styles.textDetails}>{item.securityType}</Text> */}
          {/* <FontAwesomeIcon name={""} size={20} color="firebrick" /> */}
          <Text
            style={styles.tickerName}
            numberOfLines={1}
            // ellipsizeMode="tail"
            >
            {item.quote.companyName}
          </Text>
        </View>
        {/* <View style={styles.itemViewStats}> */}
          {/* <FeatherIcon name={'sunrise'} color={theme.colors.accent} /> */}
          {/* <Text style={styles.textDetails}>Open :</Text> */}
          {/* <Text style={styles.textNumbers}>{item.marketOpen}</Text> */}
          {/* <FeatherIcon name={'sunset'} color={theme.colors.accent} /> */}
          {/* <Text style={styles.textDetails}>Close :</Text>
        <Text style={styles.textNumbers}>{item.marketClose}</Text> */}
          {/* <Text
            style={styles.tickerName}
            numberOfLines={2}
            ellipsizeMode="tail">
            {item.quote.companyName}
          </Text>
        </View> */}
       </TouchableOpacity>
      </Swipeable>
  );
};

export default withTheme(SwipableListItem);

import React from 'react';
import {
  View,
  Button,
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';
import {useStore} from './../store/store';
import {getTickerSearch} from './../actions/SearchAction';
import {addToWatchList} from './../actions/AddToWatchList';
import {addToPortfolio} from './../actions/AddToPortfolio';
import {Searchbar} from 'react-native-paper';
import {withTheme} from 'react-native-paper';
import ListItem from './ListItem';
import BottomSheet from 'reanimated-bottom-sheet';
import {Appbar} from 'react-native-paper';

const TickerSearch = ({theme}) => {
  const bs = React.createRef();
  const {state, dispatch} = useStore();
  const [searchString, setSearchString] = React.useState('');
  const [selectedItem, setSelectedItem] = React.useState();
  const OnTickerSearch = (ticker) => {
    setSearchString(ticker);
    getTickerSearch(ticker, dispatch);
  };
  const OnClear = () => {
    setSearchString(undefined);
    getTickerSearch(undefined, dispatch);
  };

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: theme.colors.surface,
      color: theme.colors.primary,
    },
    FlatList: {
      backgroundColor: theme.colors.surface,
      color: theme.colors.primary,
    },
    panelContainer: {
      position: 'absolute',
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
    },
    panel: {
      height: 600,
      padding: 20,
      backgroundColor: '#f7f5eee8',
    },
    header: {
      backgroundColor: '#f7f5eee8',
      shadowColor: '#000000',
      paddingTop: 20,
      borderTopLeftRadius: 20,
      borderTopRightRadius: 20,
    },
    panelHeader: {
      alignItems: 'center',
    },
    panelHandle: {
      width: 40,
      height: 8,
      borderRadius: 4,
      backgroundColor: '#00000040',
      marginBottom: 10,
    },
    panelTitle: {
      fontSize: 27,
      height: 35,
    },
    panelSubtitle: {
      fontSize: 14,
      color: 'gray',
      height: 30,
      marginBottom: 10,
    },
    panelButton: {
      padding: 20,
      borderRadius: 10,
      backgroundColor: '#318bfb',
      alignItems: 'center',
      marginVertical: 10,
    },
    panelButtonTitle: {
      fontSize: 17,
      fontWeight: 'bold',
      color: 'white',
    },
    noResultsContainer: {
      flex: 1,
    },
    noResultsText: {
      fontSize: 20,
      alignItems: 'center',
      justifyContent: 'center',
      textAlign: 'center',
      textAlignVertical: 'center',
      padding:20,
      color:'#bbb'
    },
  });

  const onSearchSelect = (item) => {
    // console.log('press called ', item);
    setSelectedItem(item);
    bs.current.snapTo(0);
  };

  const onAddToPortfolio = () => {
    // console.log("AddToPortfolio",selectedItem);
    if (selectedItem) {
      addToPortfolio(selectedItem.symbol, dispatch);
      onClose();
    }
  };

  const onAddToWatchList = () => {
    if (selectedItem) {
      addToWatchList(selectedItem.symbol, dispatch);
      onClose();
    }
  };

  const onClose = () => {
    bs.current.snapTo(2);
    setSelectedItem(undefined);
  };

  const renderInner = () => (
    <View style={styles.panel}>
      {/* <Text style={styles.panelTitle}>Panel Title</Text>
      <Text style={styles.panelSubtitle}>Panel sub titles</Text> */}
      <TouchableOpacity style={styles.panelButton} onPress={onAddToPortfolio}>
        <Text style={styles.panelButtonTitle}>Add to Portfolio</Text>
        {/* <Button
          title="Add to Portfolio"
          style={styles.panelButtonTitle}
          onPress={onAddToPortfolio}
        /> */}
      </TouchableOpacity>
      <TouchableOpacity style={styles.panelButton} onPress={onAddToWatchList}>
        {/* <Button
          title="Add to Watchlist"
          style={styles.panelButtonTitle}
          onPress={onAddToWatchList}
        /> */}
        <Text style={styles.panelButtonTitle}>Add to Watchlist</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.panelButton} onPress={onClose}>
        {/* <Button
          title="Close"
          style={styles.panelButtonTitle}
          onPress={onClose}
        /> */}
        <Text style={styles.panelButtonTitle}>Close</Text>
      </TouchableOpacity>
    </View>
  );

  const renderHeader = () => (
    <View style={styles.header}>
      <View style={styles.panelHeader}>
        <View style={styles.panelHandle} />
      </View>
    </View>
  );

  const renderSearchResults = () => {
    if (state.searchResults && state.searchResults.length > 0) {
      return (
        <FlatList
          data={state.searchResults}
          keyeExtractor={({item, index}) => index.toString()}
          renderItem={({item}) => (
            <ListItem
              autoFocus={true}
              OnSelect={onSearchSelect}
              keyeExtractor={item.symbol}
              item={item}
            />
          )}
        />
      );
    }

    return <Text>loading..</Text>;
  };

  return (
    <View style={styles.container}>
      {/* <Appbar.Header>
        <Appbar.Content title="Ticker Search" subtitle="Subtitle" />
      </Appbar.Header> */}
      <Searchbar
        placeholder="Search Ticker"
        value={searchString}
        onChangeText={OnTickerSearch}
        clear={OnClear}
      />
      {state.searchString && state.searchString.length > 0 ? (
        renderSearchResults()
      ) : (
        <View style={styles.noResultsContainer}>
          <Text style={styles.noResultsText}>No results yet</Text>
        </View>
      )}
      <BottomSheet
        ref={bs}
        snapPoints={[500, 250, 0]}
        renderContent={renderInner}
        renderHeader={renderHeader}
        initialSnap={2}
      />
    </View>
  );
};

export default withTheme(TickerSearch);

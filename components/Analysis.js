import React, {useEffect, useState, useCallback} from 'react';
import {useStore} from './../store/store';
import {View, StyleSheet, ScrollView, FlatList, Linking} from 'react-native';
import {Card} from 'react-native-paper';
import {
  Table,
  TableWrapper,
  Row,
  Rows,
  Col,
  Cols,
  Cell,
} from 'react-native-table-component';
import {withTheme} from 'react-native-paper';
import currencyFormatter from './../utils/currencyFormatter';
import {dateFormatter} from './../utils/dateFormatter';
import {LineChart, Grid, YAxis, XAxis} from 'react-native-svg-charts';
import {Line, Text, G, Circle, Rect} from 'react-native-svg';
import NewsItem from './NewsItem';
import * as shape from 'd3-shape';
// var currencyFormatter = new Intl.NumberFormat('en-US', {
//   style: 'currency',
//   currency: 'USD',
//   // These options are needed to round to whole numbers if that's what you want.
//   //minimumFractionDigits: 0,
//   //maximumFractionDigits: 0,
// });

const Analysis = ({route, navigation, theme}) => {
  const {ticker, details} = route.params;
  navigation.setOptions({title: ticker});
  const data = [50, 10, 40, 95, -4, -24, 85, 91, 35, 53, -53, 24, 50, -20, -80];

  const tableHead = ['Summary'];
  const tableData = [
    ['Symbol', details.quote.symbol],
    ['Name', details.quote.companyName],
    ['Price', currencyFormatter.format(details.quote.latestPrice)],
    ['Trade Date', details.quote.latestTime],
    ['Open', currencyFormatter.format(details.quote.open)],
    ['Close', currencyFormatter.format(details.quote.close)],
    ['High', currencyFormatter.format(details.quote.high)],
    ['Low', currencyFormatter.format(details.quote.low)],
    ['52 week high', currencyFormatter.format(details.quote.week52High)],
    ['52 week low', currencyFormatter.format(details.quote.week52Low)],
    ['Market Cap', currencyFormatter.format(details.quote.marketCap)],
    ['Volume', currencyFormatter.format(details.quote.volume)],
    ['P/E ratio', details.quote.peRatio],
    ['YTD Change', currencyFormatter.format(details.quote.ytdChange)],
  ];
  const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    textLabel: {
      fontSize: 5,
      color: 'red',
    },
    textValue: {
      fontSize: 5,
    },
    head: {height: 40, backgroundColor: '#c8e1ff', color: theme.colors.primary},
    text: {margin: 6},
  });

  const CUT_OFF = 20;
  const Labels = ({x, y, bandwidth, data}) =>
    data.map((value, index) => (
      <Text
        key={index}
        x={x(index)}
        y={y(value)} // {value < CUT_OFF ? y(value) - 10 : y(value) + 15}
        fontSize={9}
        fill="black" // {value >= CUT_OFF ? 'white' : 'black'}
        // alignmentBaseline={'middle'}
        // textAnchor={'middle'}
      >
        {value}
      </Text>
    ));

  const Tooltip = ({x, y, data}) => {
    return data.map((value, index) => (
      <Circle
        cy={y(value)}
        cx={x(index)}
        r={6}
        stroke={'rgb(134, 65, 244)'}
        strokeWidth={2}
        fill={'white'}
        onPress={ () => console.log('tooltip clicked') }
      >
        <Text key={index} fill="black" fontSize={5}>{value}</Text>
      </Circle>
    ));
    // <G
    //     x={ x(5) - (75 / 2) }
    //     key={ 'tooltip' }
    //     onPress={ () => console.log('tooltip clicked') }
    // >
    //     <G y={ 50 }>
    //         <Rect
    //             height={ 40 }
    //             width={ 75 }
    //             stroke={ 'grey' }
    //             fill={ 'white' }
    //             ry={ 10 }
    //             rx={ 10 }
    //         />
    //         <Text
    //             x={ 75 / 2 }
    //             dy={ 20 }
    //             alignmentBaseline={ 'middle' }
    //             textAnchor={ 'middle' }
    //             stroke={ 'rgb(134, 65, 244)' }
    //         >
    //             { `${data[5]}ºC` }
    //         </Text>
    //     </G>
    //     <G x={ 75 / 2 }>
    //         <Line
    //             y1={ 50 + 40 }
    //             y2={ y(data[ 5 ]) }
    //             stroke={ 'grey' }
    //             strokeWidth={ 2 }
    //         />
    //         <Circle
    //             cy={ y(data[ 5 ]) }
    //             r={ 6 }
    //             stroke={ 'rgb(134, 65, 244)' }
    //             strokeWidth={ 2 }
    //             fill={ 'white' }
    //         />
    //     </G>
    // </G>
  };
  const OnNewsSelect=(item)=> {
    const handlePress = useCallback(async () => {
      // Checking if the link is supported for links with custom URL scheme.
      const supported = await Linking.canOpenURL(item.url);
  
      if (supported) {
        // Opening the link with some app, if the URL scheme is "http" the web link should be opened
        // by some browser in the mobile
        await Linking.openURL(item.url);
      } else {
        Alert.alert(`Don't know how to open this URL: ${item.url}`);
      }
    }, [item])

    return handlePress();
  };

  return (
    <ScrollView>
      <Card>
        <Card.Title title="Chart" />
        <Card.Content>
          <View>
            <View style={{height: 200, flexDirection: 'row'}}>
              <YAxis
                data={data}
                contentInset={{top: 20, bottom: 20}}
                svg={{
                  fill: 'grey',
                  fontSize: 10,
                }}
                numberOfTicks={10}
                formatLabel={(value) => `${value}`}
              />
              <LineChart
                style={{flex: 1, marginLeft: 16}}
                data={data}
                svg={{stroke: 'rgb(134, 65, 244)', strokeWidth: 2}}
                animate={true}
                animationDuration={300}
                contentInset={{top: 20, bottom: 20}}
                curve={ shape.curveLinear }>
                <Grid />
                {/* <Labels/> */}
                <Tooltip />
              </LineChart>
            </View>
            <XAxis
              // style={{marginHorizontal: -20}}
              data={data}
              formatLabel={(value, index) => index}
              contentInset={{left: 35, right: 5}}
              svg={{fontSize: 10, fill: 'black'}}
            />
          </View>
        </Card.Content>
      </Card>
      <Card>
        <Card.Title title="Summary" />
        <Card.Content>
          <View style={styles.container}>
            <Table borderStyle={{borderWidth: 2, borderColor: '#c8e1ff'}}>
              {/* <Row
                  data={tableHead}
                  style={styles.head}
                  textStyle={styles.text}
                /> */}
              <Rows data={tableData} textStyle={styles.text} />
            </Table>
          </View>
        </Card.Content>
      </Card>
      <Card>
        <Card.Title title="News" />
        <Card.Content>
          <View style={styles.container}>
            <FlatList 
                data={details.news}
                keyeExtractor={({item, index})=>index.toString()}
                renderItem={({item}) =>(
                  <NewsItem
                    OnSelect={OnNewsSelect}
                    item={item}
                    />
                )}
            />            
          </View>
        </Card.Content>
      </Card>
    </ScrollView>
  );
};

export default withTheme(Analysis);

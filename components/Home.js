import React, {useEffect, useState} from 'react';
import {useStore} from './../store/store';
import {View, Text, Button, StyleSheet} from 'react-native';
import {withTheme} from 'react-native-paper';
import { getPortfolio } from '../actions/LoadPortfolio';
import _ from 'lodash';
import {FlatList} from 'react-native-gesture-handler';
import SwipableListItem from './SwipableListItem';
import { Appbar } from 'react-native-paper';

const Home = ({navigation, theme}) => {
  const {state, dispatch} = useStore();
  const [portfolioList, setPortfolioList] = useState({});
  
  useEffect(()=>navigation.addListener('focus', () => {
    // loadData(state, setPortfolioList, dispatch);
    getPortfolio(dispatch);
  },[dispatch,state.portfolio]));

  useEffect(()=>{
    loadData(state, setPortfolioList, dispatch);
  },[dispatch,state.portfolio]);

  const loadData=(state, setPortfolioList, dispatch) => {
    if (!_.isEmpty(state.portfolio)) {
      console.log(state.portfolio);
      const tickers = Object.keys(state.portfolio);
      const finalPortfolioList = tickers.map((x) => {
        return {
          ticker: x,
          ...state.portfolio[x]
        };
      });
      setPortfolioList(finalPortfolioList);
    }
    // else {
    //   getPortfolio(dispatch);
    // }
  }
  
  const onSelect = (item) => {
    console.log(item);
  };

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: theme.colors.surface,
      color: theme.colors.primary,
      // alignItems: 'center',
      // justifyContent: 'center',
    },
    touchableContainer: {
      padding: 15,
      backgroundColor: theme.colors.surface,
      borderBottomWidth: 1,
      borderBottomColor: '#333',
    },
    itemView: {
      flexDirection: 'row',
      // justifyContent: 'space-between',
      alignItems: 'center',
    },
    itemViewStats: {
      flexDirection: 'row',
      // justifyContent: 'space-around',
      padding: 3,
      margin: 3,
      fontSize: 10,
      color: theme.colors.primary,
    },
    ticker: {
      color: theme.colors.accent,
      backgroundColor: theme.colors.primary,
      // fontSize: 14,
      margin: 1,
    },
    tickerName: {
      // color: '#ffffff',
      color: theme.colors.accent,
      fontSize: 14,
      textDecorationStyle: 'dotted',
      flexWrap: 'wrap',
      marginLeft: 2,
      marginRight: 1,
    },
  });

  return (
    <View style={styles.container}>
      {/* <Appbar.Header>
        <Appbar.Content title="Portfolio" subtitle="Subtitle" />
      </Appbar.Header> */}
      {
        _.isEmpty(portfolioList) 
        ?
        <View>
          <Text style={{color: theme.colors.primary}}>Home Screen</Text>
          <Button
            title="Go to Search"
            style={{color: theme.colors.primary}}
            onPress={() => navigation.navigate('Search')}
          />
        </View>          
        :
        <FlatList
          data={portfolioList}
          keyeExtractor={({item, index})=>index.toString()}
          renderItem={({item,index}) => (
            <SwipableListItem
              // key={index.toString()}
              autoFocus={true}
              OnSelect={onSelect}
              keyeExtractor={item.symbol}
              item={item}
            />
          )}
        />
      }

    </View>
  );
};

export default withTheme(Home);


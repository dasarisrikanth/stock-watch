import React, {useEffect, useState} from 'react';
import {useStore} from './../store/store';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import {withTheme} from 'react-native-paper';
import {getWatchList} from './../actions/LoadWatchlist';
import SwipableListItem from './SwipableListItem';
import {Appbar} from 'react-native-paper';
import _ from 'lodash';
import {FlatList} from 'react-native-gesture-handler';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import {removeFromWatchlist} from './../actions/RemoveFromWatchlist';
import  {usePrevious} from './../utils/usePrevious';

const WatchList = ({navigation, theme}) => {
  const {state, dispatch} = useStore();
  const [watchList, setWatchList] = useState({});
  const previousState = usePrevious(state); // it causing some issue


  useEffect(()=>navigation.addListener('focus', () => {
    // loadData(state, setPortfolioList, dispatch);
    getWatchList(dispatch);
  },[dispatch,state.portfolio]));

  useEffect(() => {
    console.log("previousState",previousState);
    if (!_.isEmpty(state.watchList) && previousState && previousState.watchList !== state.watchList) {
      // console.log(state.watchList);
      const tickers = Object.keys(state.watchList);
      const finalWatchlist = tickers.map((x) => {
        return {
          ticker: x,
          ...state.watchList[x],
        };
      });
      // console.log(finalWatchlist);
      setWatchList(finalWatchlist);
    } 
    // else {
    //   getWatchList(dispatch);
    // }
  }, [dispatch, state.watchList]);

  const onSelect = (item) => {
    console.log("onselect",item);
    // return <Analysis />;
    navigation.navigate('Analysis',{ticker:item.ticker, details: item});
  };

  const onDelete = (item) => {
    console.log(item);
    if(item && item.ticker) {
      removeFromWatchlist(item.ticker, dispatch);
      console.log("deleted");
      console.log(dispatch);
      // getWatchList(dispatch);
      const afterDelete = _.remove(watchList,(x)=>{
        return x.ticker.toLowerCase() === item.ticker.toLowerCase();
      });
      setWatchList(afterDelete);
    }    
  };
  

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: theme.colors.surface,
      color: theme.colors.primary,
    },
    touchableContainer: {
      padding: 15,
      backgroundColor: theme.colors.surface,
      borderBottomWidth: 1,
      borderBottomColor: '#333',
    },
    itemView: {
      flexDirection: 'row',
      // justifyContent: 'space-between',
      alignItems: 'center',
    },
    itemViewStats: {
      flexDirection: 'row',
      // justifyContent: 'space-around',
      padding: 3,
      margin: 3,
      fontSize: 10,
      color: theme.colors.primary,
    },
    ticker: {
      color: theme.colors.accent,
      backgroundColor: theme.colors.primary,
      // fontSize: 14,
      margin: 1,
    },
    tickerName: {
      // color: '#ffffff',
      color: theme.colors.accent,
      fontSize: 14,
      textDecorationStyle: 'dotted',
      flexWrap: 'wrap',
      marginLeft: 2,
      marginRight: 1,
    },
  });

  return (
    <View style={styles.container}>
      {/* {watchList && watchList.length > 0 ? (
        watchList.map((x) => (
          <TouchableOpacity
            keyExtract={x.ticker}
            style={styles.touchableContainer}>
            <Text>{x.ticker}</Text>
          </TouchableOpacity>
        ))
      ) : (
        <Text style={styles.tickerName}>No Watchlist</Text>
      )} */}
      {/* <Appbar.Header>
        <Appbar.Content title="Watchlist" subtitle="Subtitle" />
      </Appbar.Header> */}
      {_.isEmpty(watchList) ? (
        <View>
          <Text>No Items</Text>
        </View>
      ) : (
        <FlatList
          data={watchList}
          keyeExtractor={({item, index})=>index.toString()}
          renderItem={({item}) => (
            
              <SwipableListItem
                autoFocus={true}
                OnSelect={onSelect}
                keyeExtractor={item.symbol}
                item={item}
                // onSwipeFromLeft={() => alert('swiped from left!')}
                onRightPress={onDelete}
              />
            
          )}
        />
      )}
    </View>
  );
};

export default withTheme(WatchList);

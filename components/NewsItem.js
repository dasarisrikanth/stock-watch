import React, {useState} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  FlatList,
  TextInput,
  TouchableOpacity,
} from 'react-native';

import {withTheme} from 'react-native-paper';

const NewsItem = ({item, theme, OnSelect}) => {
  const [selectedItem, setSelectedItem] = useState();
  const styles = StyleSheet.create({
    container: {
      flex:1,
      // padding:15,
      // backgroundColor: theme.colors.surface,
      // borderBottomWidth: 1,
      // borderBottomColor: '#333',
    },
    itemView : {
      flexDirection:'row',
      padding:5,
      backgroundColor: theme.colors.surface,
      borderBottomWidth: 1,
      borderBottomColor: '#bbb',
    },
    headLine: {
      color: theme.colors.primary,
    },
    tinyLogo: {
      width: 60,
      height: 60,
    },
    tinyText: {
      color:'grey',
      fontSize:7
    }
  });

  return (
    <TouchableOpacity
      style={styles.conatiner}
      onPress={() => OnSelect(item)}
    >
      <View style={styles.itemView}>
        <View>
            <Image style={styles.tinyLogo} source={{uri:item.image}} />
        </View>
        <View style={{flexDirection: 'column'}}>
          <Text style={styles.headLine}>{item.headline}</Text>
          <Text style={styles.tinyText}>{item.source} - {new Date(item.datetime).toLocaleDateString("en-US")}</Text>
        </View>
        
      </View>      
    </TouchableOpacity>
  )
}

export default withTheme(NewsItem);
import React, { useEffect } from 'react';
import {StyleSheet, YellowBox } from 'react-native';
import {withTheme} from 'react-native-paper';
import RootComponent from './components/RootComponent';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Home from './components/Home';
import TickerSearch from './components/TickerSearch';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import WatchList from './components/WatchList';
import Analysis from './components/Analysis';

const WatchListStack = createStackNavigator();
const PortfolioStack = createStackNavigator();
const TickerSearchStack = createStackNavigator();
const Tab = createBottomTabNavigator();

const Main = ({theme}) => {
  // console.disableYellowBox = true
  // YellowBox.ignoreWarnings(['Warning:'])
  return (
    <NavigationContainer theme={theme}>
      <Tab.Navigator
        screenOptions={({route}) => ({
          tabBarIcon: ({focused, color, size}) => {
            let iconName;
            if (route.name === 'Portfolio') {
              iconName = 'ios-home';
            } else if (route.name === 'Search') {
              iconName = 'ios-search';
            } else if (route.name === 'Watchlist') {
              iconName = 'ios-star';
            }

            // You can return any component that you like here!
            return <Ionicons name={iconName} size={size} color={color} />;
          },
        })
        
      }
        tabBarOptions={{
          activeTintColor: theme.colors.primary,
          inactiveTintColor: theme.colors.secondary,
          activeBackgroundColor: theme.colors.surface,
          inactiveBackgroundColor: theme.colors.surface,
        }}>
        <Tab.Screen name="Portfolio" component={portfolioStack} />
        <Tab.Screen name="Watchlist" component={watchListStack} theme={theme}/>
        <Tab.Screen name="Search" component={tickerSearchStack} />
      </Tab.Navigator>
    </NavigationContainer>
  );
};

const watchListStack = ({theme}) => {
  return (
    <WatchListStack.Navigator>
      <WatchListStack.Screen name="Watchlist" component={WatchList} options={{
                  headerStyle: {
                    backgroundColor: '#3498db',
                  },          
                  headerTintColor: '#fff',
                  headerTitleStyle: {
                    fontWeight: 'bold',
                  },
      }} />
      <WatchListStack.Screen name="Analysis" theme={theme} component={Analysis} options={{
                  headerStyle: {
                    backgroundColor: '#3498db',
                  },          
                  headerTintColor: '#fff',
                  headerTitleStyle: {
                    fontWeight: 'bold',
                  },
      }} />
    </WatchListStack.Navigator>
  )
}

const portfolioStack = () => {
  return (
    <PortfolioStack.Navigator>
      <PortfolioStack.Screen name="Portfolio" component={Home} options={{
                  headerStyle: {
                    backgroundColor: '#3498db',
                  },          
                  headerTintColor: '#fff',
                  headerTitleStyle: {
                    fontWeight: 'bold',
                  },
      }} />
    </PortfolioStack.Navigator>
  )
}

const tickerSearchStack = () => {
  return (
    <TickerSearchStack.Navigator>
      <TickerSearchStack.Screen name="Ticker Search" component={TickerSearch} options={{
                  headerStyle: {
                    backgroundColor: '#3498db',
                  },          
                  headerTintColor: '#fff',
                  headerTitleStyle: {
                    fontWeight: 'bold',
                  },
      }}/>
    </TickerSearchStack.Navigator>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default withTheme(Main);

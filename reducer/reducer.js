import {
  SEARCH_INPROGRESS,
  SEARCH_ERROR,
  SEARCH_SUCCESS,
  SEARCH_STRING,
  LOAD_WATCHLIST_ERROR,
  LOAD_WATCHLIST_INPROGRESS,
  LOAD_WATCHLIST_SUCCESS,
  ADD_TO_WATCHLIST_INPROGRESS,
  ADD_TO_WATCHLIST_ERROR,
  ADD_TO_WATCHLIST_SUCCESS,
  ADD_TO_PORTFOLIO_INPROGRESS,
  ADD_TO_PORTFOLIO_ERROR,
  ADD_TO_PORTFOLIO_SUCCESS,
  LOAD_PORTFOLIO_INPROGRESS,
  LOAD_PORTFOLIO_ERROR,
  LOAD_PORTFOLIO_SUCCESS,
  REMOVE_FROM_PORTFOLIO_ERROR,
  REMOVE_FROM_PORTFOLIO_INPROGRESS,
  REMOVE_FROM_WATCHLIST_INPROGRESS,
  REMOVE_FROM_WATCHLIST_ERROR,
  REMOVE_FROM_WATCHLIST_SUCCESS,
  REMOVE_FROM_PORTFOLIO_SUCCESS
} from './../constants/ActionTypes';
import {initialState} from './../store/store';

const reducer = (state = initialState, {type, data, error, ticker} = {}) => {
  switch (type) {
    case LOAD_WATCHLIST_INPROGRESS:
    case LOAD_PORTFOLIO_INPROGRESS:
    case SEARCH_INPROGRESS:
    case ADD_TO_WATCHLIST_INPROGRESS:
    case ADD_TO_PORTFOLIO_INPROGRESS:
    case REMOVE_FROM_PORTFOLIO_INPROGRESS:
    case REMOVE_FROM_WATCHLIST_INPROGRESS:
      return {
        ...state,
        status: type,
      };
    case LOAD_WATCHLIST_ERROR:
    case SEARCH_ERROR:
    case ADD_TO_WATCHLIST_ERROR:
    case ADD_TO_PORTFOLIO_ERROR:
    case LOAD_PORTFOLIO_ERROR:
    case REMOVE_FROM_PORTFOLIO_ERROR:
    case REMOVE_FROM_WATCHLIST_ERROR:
      return {
        ...state,
        status: type,
        error,
      };
    case SEARCH_SUCCESS:
      return {
        ...state,
        status: SEARCH_SUCCESS,
        searchResults: data,
      };
    case SEARCH_STRING:
      return {
        ...state,
        status: SEARCH_STRING,
        searchString: ticker,
      };
    case LOAD_WATCHLIST_SUCCESS:
      return {
        ...state,
        status: type,
        watchList: data,
      };
    case ADD_TO_WATCHLIST_SUCCESS:
      return {
        ...state,
        status: type,
      };    
    case ADD_TO_PORTFOLIO_SUCCESS:
      return {
        ...state,
        status:type
      };
    case LOAD_PORTFOLIO_SUCCESS:
      return {
        ...state,
        portfolio:data,
        status: type,        
      };
    case REMOVE_FROM_WATCHLIST_SUCCESS:
    case REMOVE_FROM_PORTFOLIO_SUCCESS:
      return {
        ...state,
        status:type
      };
    default:
      return state;
  }
};

export default reducer;

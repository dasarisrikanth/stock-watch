const isSandbox = true;
export const SECRET_TOKEN = '';

export const BASE_URL = isSandbox
  ? 'https://sandbox.iexapis.com'
  : 'https://cloud.iexapis.com';

export const ENDPOINT = {
  SEARCH_ENDPOINT: '/stable/search/${fragment}',
  WATCHLIST_DETAILS: '/stable/stock/market/batch/',
};

export const dateFormatter = (unix_timestamp) => {
  
  const date = new Date(unix_timestamp);
  var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  // // Hours part from the timestamp
  // const hours = date.getHours();
  // // Minutes part from the timestamp
  // const minutes = "0" + date.getMinutes();
  // // Seconds part from the timestamp
  // const seconds = "0" + date.getSeconds();
  
  // const year = date.getFullYear();
  // const month =  months[date.getMonth()];
  // const _date = date.getDate();

  // return _date + '-' + month + '-' + year;
  return date.toLocaleDateString("en-US");
}
import AsyncStorage from '@react-native-community/async-storage';
// var AsyncLock = require('async-lock');
// var lock = new AsyncLock({timeout: 5000});

export const storeData = async (key, value) => {
  // await lock.acquire(key,async function(done){
  //   console.log("storeData lock entered");
    try {
      const jsonValue = JSON.stringify(value);
      await AsyncStorage.setItem(key, jsonValue);
    } catch (e) {
      // saving error
      console.log(e);
    }
  // },function(err, ret){
  //   console.log("storeData lock released");
  // });  
};

export const getData = async (key) => {
  // console.log("** lock.isBusy() ",lock.isBusy());
  // while(lock.isBusy());
  // console.log("*********lock is not busy***********");
  try {
    const jsonValue = await AsyncStorage.getItem(key);
    console.log("*****",jsonValue);
    return jsonValue != null ? JSON.parse(jsonValue) : null;
  } catch (e) {
    // error reading value
    console.log(e);
  }
};


export const removeItemValue = async (key) => {
  try {
    console.log("key",key);
    await AsyncStorage.removeItem(key);
    AsyncStorage.getAllKeys((err, keys) =>{
      console.log("removeItemValue ->  after removal key ",keys)
    });
    return true;
  } catch (exception) {
    return false;
  }
};

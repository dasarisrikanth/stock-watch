import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {name as appName} from './app.json';
import {
  DefaultTheme,
  DarkTheme,
  Provider as PaperProvider,
} from 'react-native-paper';
import {StoreProvider} from './store/store';
import Main from './Main';
import('./utils/ReactotronConfig').then(() =>
  console.log('Reactotron Configured'),
);
const theme = {
  // ...DarkTheme,
  ...DefaultTheme,
  roundness: 2,
  dark: true,
  colors: {
    // ...DarkTheme.colors,
    ...DefaultTheme.colors,
    primary: '#3498db',
    accent: '#2590f5',
  },
};

export default function App() {
  return (
    <StoreProvider>
      <PaperProvider theme={theme}>
        <Main />
        <StatusBar style="auto" />
      </PaperProvider>
    </StoreProvider>
  );
}

